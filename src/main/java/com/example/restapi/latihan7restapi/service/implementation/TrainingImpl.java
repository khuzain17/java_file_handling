package com.example.restapi.latihan7restapi.service.implementation;

import com.example.restapi.latihan7restapi.entity.Training;
import com.example.restapi.latihan7restapi.repository.TrainingRepo;
import com.example.restapi.latihan7restapi.service.TrainingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
@Transactional
public class TrainingImpl implements TrainingService {
    @Autowired
    public TrainingRepo repo;

    @Override
    public Map insert(Training training) {
        Map map = new HashMap();
        try {
            Training train = repo.save(training);
            map.put("data", train);
            map.put("statusCode", "200");
            map.put("statusMessage", "Training created successfully");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("statusCode", "500");
            map.put("statusMessage", e);
            return map;
        }
    }

    @Override
    public Map update(Training training) {
        Map map = new HashMap();
        try {
            Training train = repo.getById(training.getId());

            if (train == null) {
                map.put("statusCode", "404");
                map.put("statusMessage", "Training does not exist");
                return map;
            }

            train.setNamaPengajar(training.getNamaPengajar());
            train.setTema(training.getTema());
            repo.save(train);

            map.put("data", train);
            map.put("statusCode", "200");
            map.put("statusMessage", "Training updated successfully");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("statusCode", "500");
            map.put("statusMessage", e);
            return map;
        }
    }

    @Override
    public Map delete(Long idTraining) {
        Map map = new HashMap();
        try {
            Training train = repo.getById(idTraining);

            if (train == null) {
                map.put("statusCode", "404");
                map.put("statusMessage", "Training does not exist");
                return map;
            }
            train.setDeleted_date(new Date());
            repo.save(train);

            map.put("statusCode", "200");
            map.put("statusMessage", "Training deleted successfully");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("statusCode", "500");
            map.put("statusMessage", e);
            return map;
        }
    }

    @Override
    public Map getById(Long idTraining) {
        Map map = new HashMap();
        try {
            Training train = repo.getById(idTraining);

            if (train == null) {
                map.put("statusCode", "404");
                map.put("statusMessage", "Training does not exist");
                return map;
            }

            map.put("data", train);
            map.put("statusCode", "200");
            map.put("statusMessage", "Data Training");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("statusCode", "500");
            map.put("statusMessage", e);
            return map;
        }
    }

    @Override
    public Map getAll() {
        List<Training> trainings = new ArrayList<Training>();
        Map map = new HashMap();
        try {
            trainings = repo.getAllTraining();
            map.put("data", trainings);
            map.put("statusCode", "200");
            map.put("statusMessage", "Success");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("statusCode", "500");
            map.put("statusMessage", e);
            return map;
        }
    }
}
