package com.example.restapi.latihan7restapi.service;

import com.example.restapi.latihan7restapi.entity.EmployeeDetail;
import com.example.restapi.latihan7restapi.entity.Rekening;

import java.util.Map;

public interface EmployeeDetailService {
    public Map insert(EmployeeDetail employeeDetail);

    public Map update(EmployeeDetail employeeDetail);

    public Map delete(Long idEmployeeDetail);

    public Map getById(Long idEmployeeDetail);

    public Map getAll();
}
