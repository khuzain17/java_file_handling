package com.example.restapi.latihan7restapi.service;

import com.example.restapi.latihan7restapi.entity.Training;

import java.util.Map;

public interface TrainingService {
    public Map insert(Training training);

    public Map update(Training training);

    public Map delete(Long idTraining);

    public Map getById(Long idTraining);

    public Map getAll();

}
