package com.example.restapi.latihan7restapi.service;

import com.example.restapi.latihan7restapi.entity.Rekening;
import com.example.restapi.latihan7restapi.entity.Training;

import java.util.Map;

public interface RekeningService {
    public Map insert(Rekening rekening);

    public Map update(Rekening rekening);

    public Map delete(Long idRekening);

    public Map getById(Long idRekening);

    public Map getAll();
}
