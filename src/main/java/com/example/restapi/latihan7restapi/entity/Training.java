package com.example.restapi.latihan7restapi.entity;

import com.example.restapi.latihan7restapi.entity.Abstract.AbstractDate;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Setter
@Getter
@Entity
@Table(name = "training")
public class Training extends AbstractDate implements Serializable {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "tema", length = 35)
    private String tema;

    @Column(name = "nama_pengajar", length = 35)
    private String namaPengajar;

    @JsonIgnore
    @OneToMany(mappedBy = "training")
    private List<EmployeeTraining> employeeTraining;
}
