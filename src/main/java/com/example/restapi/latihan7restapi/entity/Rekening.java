package com.example.restapi.latihan7restapi.entity;

import com.example.restapi.latihan7restapi.entity.Abstract.AbstractDate;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.autoconfigure.ldap.embedded.EmbeddedLdapAutoConfiguration;

import javax.persistence.*;
import java.io.Serializable;

@Setter
@Getter
@Entity
@Table(name = "rekening")
public class Rekening extends AbstractDate implements Serializable {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "nama_bank", length = 20)
    private String namaBank;

    @Column(name = "jenis", length = 20)
    private String jenis;

    @Column(name = "nomor", length = 25)
    private String nomor;

    @ManyToOne(targetEntity = Employee.class, cascade = CascadeType.ALL)
    private Employee employee;
}
