package com.example.restapi.latihan7restapi.entity;

import com.example.restapi.latihan7restapi.entity.Abstract.AbstractDate;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Setter
@Getter
@Entity
@Table(name = "employee_detail")
public class EmployeeDetail extends AbstractDate implements Serializable {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "nik", length = 35)
    private String nik;

    @Column(name = "npwp", length = 35)
    private String npwp;

    @OneToOne(targetEntity = Employee.class, cascade = CascadeType.ALL)
    private Employee employee;

}
