package com.example.restapi.latihan7restapi.controller;

import com.example.restapi.latihan7restapi.entity.Rekening;
import com.example.restapi.latihan7restapi.entity.Training;
import com.example.restapi.latihan7restapi.repository.RekeningRepo;
import com.example.restapi.latihan7restapi.service.RekeningService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("synrgy/rekening")
public class RekeningController {

    @Autowired
    RekeningService service;

    @Autowired
    public RekeningRepo rekeningRepo;

    @GetMapping("/all")
    @ResponseBody
    public ResponseEntity<Map> getAll() {
        Map rekenings = service.getAll();
        return new ResponseEntity<Map>(rekenings, HttpStatus.OK);
    }

    //sorting by @requestParam
    @GetMapping("/all-sorting")
    @ResponseBody
    public ResponseEntity<Map> getAllSorting(
            @RequestParam() Integer page,
            @RequestParam() Integer size,
            @RequestParam() String nameShort,
            @RequestParam() String typeShort
    ) {
        Map map = new HashMap();
        String getshorting = nameShort.equals("")?"id": nameShort;
        Pageable show_data;
        if(typeShort.equals("desc")) {
            show_data = PageRequest.of(page, size, Sort.by(getshorting).descending());
        } else  {
            show_data = PageRequest.of(page, size, Sort.by(getshorting).ascending());
        }
        map.put("data", rekeningRepo.findAll(show_data));
        return new ResponseEntity<Map>(map, new HttpHeaders(), HttpStatus.OK);
    }

    @GetMapping("/all/{id}")
    @ResponseBody
    public ResponseEntity<Map> getById(@PathVariable(value = "id") Long id) {
        Map rek = service.getById(id);
        return new ResponseEntity<Map>(rek, HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Map> delete(@PathVariable(value = "id") Long id) {
        Map rekening = service.delete(id);
        return new ResponseEntity<Map>(rekening, HttpStatus.OK);
    }

    @PutMapping("/update")
    public ResponseEntity<Map> update(@Valid @RequestBody Rekening rekeningToUpdate) {
        Map map = new HashMap();
        Map rekening = service.update(rekeningToUpdate);

        map.put("Request = ", rekeningToUpdate);
        map.put("Response = ", rekening);
        return new ResponseEntity<Map>(rekening, HttpStatus.OK);
    }

    @PostMapping("/save")
    public ResponseEntity<Map> save(@Valid @RequestBody Rekening newRekening) {
        Map map = new HashMap();
        Map rek = service.insert(newRekening);

        map.put("Request = ", newRekening);
        map.put("Response = ", rek);
        return new ResponseEntity<Map>(rek, HttpStatus.OK);
    }
}
