package com.example.restapi.latihan7restapi.repository;

import com.example.restapi.latihan7restapi.entity.Training;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TrainingRepo extends JpaRepository<Training, Long> {
    @Query("select e from Training e where e.id = :id")
    public Training getById(@Param("id") Long id);

    @Query("select e from Training e")
    public List<Training> getAllTraining();
}
