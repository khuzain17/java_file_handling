package com.example.restapi.latihan7restapi.repository;

import com.example.restapi.latihan7restapi.entity.Employee;
import com.example.restapi.latihan7restapi.entity.Rekening;
import com.example.restapi.latihan7restapi.entity.Training;
import com.example.restapi.latihan7restapi.service.implementation.RekeningImpl;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RekeningRepo extends JpaRepository<Rekening, Long> {
    @Query("select e from Rekening e where e.id = :id")
    public Rekening getById(@Param("id") Long id);

    @Query("select e from Rekening e")
    public List<Rekening> getAllRekening();
}
